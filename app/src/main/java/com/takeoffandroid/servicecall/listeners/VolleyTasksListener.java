package com.takeoffandroid.servicecall.listeners;

import org.json.JSONArray;
import org.json.JSONObject;

public interface VolleyTasksListener {

    void handleError(int requestCode,String error);
	void handleResult(int requestCode, JSONArray response);
    void handleResult(int requestCode, JSONObject response);

}
