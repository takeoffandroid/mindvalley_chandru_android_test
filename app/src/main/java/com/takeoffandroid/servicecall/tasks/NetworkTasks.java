package com.takeoffandroid.servicecall.tasks;

import android.app.Activity;
import android.content.Context;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.takeoffandroid.servicecall.BaseApplication;
import com.takeoffandroid.servicecall.listeners.VolleyTasksListener;
import com.takeoffandroid.servicecall.util.Logger;
import com.takeoffandroid.servicecall.views.CustomProgressDialog;

import org.json.JSONArray;
import org.json.JSONObject;


public class NetworkTasks {
	
	
	static VolleyTasksListener mVolley;

	private static String TAG = NetworkTasks.class.getSimpleName();

	private static CustomProgressDialog mProgressDialog;



	public static void callVolleyGETArray(Context mContext, final VolleyTasksListener listener, String url, final int requestCode) {

		mProgressDialog = new CustomProgressDialog(mContext);
		showpDialog();

		JsonArrayRequest jsonObjReq = new JsonArrayRequest(
				url, new Response.Listener<JSONArray>() {

					@Override
					public void onResponse(JSONArray response) {
						Logger.d (response.toString ());

                        hidepDialog();


						listener.handleResult(requestCode,response);
                    }
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(TAG, "Error: " + error.getMessage());
                        hidepDialog();

						listener.handleError(requestCode,error.getMessage());

					}
				});

        BaseApplication.getInstance().addToRequestQueue(jsonObjReq);
	}


//	public static void callVolleyGETArr(final Activity context, String url, final int requestCode) {
//		mVolley = (VolleyTasksListener) context;
//
//		mProgressDialog = new CustomProgressDialog(context);
//		showpDialog();
//
//		JsonArrayRequest jsonObjReq = new JsonArrayRequest(
//				url, new Response.Listener<JSONArray>() {
//
//			@Override
//			public void onResponse(JSONArray response) {
//				Logger.d (response.toString ());
//
//                        hidepDialog();
//
//
//				mVolley.handleResult(requestCode,response);
//			}
//		}, new Response.ErrorListener() {
//
//			@Override
//			public void onErrorResponse(VolleyError error) {
//				VolleyLog.d(TAG, "Error: " + error.getMessage());
//						hidepDialog();
//
//				mVolley.handleError(requestCode,error.getMessage());
//
//			}
//		});
//
//		BaseApplication.getInstance().addToRequestQueue(jsonObjReq);
//	}
//


	public static void callVolleyGETObject(Context context, final VolleyTasksListener listener,String url,final int requestCode) {

		mProgressDialog = new CustomProgressDialog(context);
        showpDialog();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Logger.d (response.toString ());

                hidepDialog();

				mVolley.handleResult(requestCode,response);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidepDialog();
				mVolley.handleError(requestCode,error.getMessage());

			}
        });

        // Adding request to request queue
        BaseApplication.getInstance().addToRequestQueue(jsonObjReq);
    }

	
	private static void showpDialog() {
		if (!mProgressDialog.isShowing())
			mProgressDialog.showDialog("");
	}

	private static void hidepDialog() {
        if(mProgressDialog!=null) {
			mProgressDialog.dismissDialog();
        }
    }

	public static void loadImage(Context context, String url, ImageView imageView){

		Glide.with(context)
				.load(url)
				.centerCrop()
				.crossFade()
				.into(imageView);
	}
}
