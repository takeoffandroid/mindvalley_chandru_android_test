package com.takeoffandroid.servicecall.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import com.takeoffandroid.servicecall.models.AbstractUser;
import com.takeoffandroid.servicecall.util.Logger;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.ArrayList;


public class DBAccess extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static String DATABASE_NAME="database";

    //UserMaster Table
    public static final String TABLE_USER_DATA = "table_userdata";

    public static final String KEY_USER_DATA = "UserData";


    public static final String CREATE_TABLE_USER_DATA ="CREATE TABLE IF NOT EXISTS "+TABLE_USER_DATA+ "("+KEY_USER_DATA+ " BLOB);";



    private static DBAccess mInstance;

    public DBAccess (Context context) {
        super (context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static DBAccess init(Context context) {


        if (mInstance == null)
            synchronized (DBAccess.class) {
                if (mInstance == null) {
                    mInstance = new DBAccess(context);

                }
            }
        return mInstance;
    }

    @Override
    public void onCreate (SQLiteDatabase db) {

        Logger.i("CREATE_TABLE_USER_DATA");
        db.execSQL (CREATE_TABLE_USER_DATA);


    }

    @Override
    public void onUpgrade (SQLiteDatabase db, int oldVersion, int newVersion) {


    }


    public void insertUserData(ArrayList<AbstractUser> abstractUser) {

        SQLiteDatabase SqlDB = getWritableDatabase();

        SqlDB.delete(TABLE_USER_DATA, null, null);

        String INSERT = "insert into "+TABLE_USER_DATA+ " ("+KEY_USER_DATA+") values (?)";
        SQLiteStatement insertstatment = SqlDB.compileStatement(INSERT);
        insertstatment.bindBlob(1, convertIntoBLOB(abstractUser));
        insertstatment.executeInsert();
        SqlDB.close();
        close();

    }

    public ArrayList<AbstractUser> getUserData() {

        ArrayList<AbstractUser> abstractUser = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(TABLE_USER_DATA, new String[] { KEY_USER_DATA },
                null, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            if (cursor.getCount() > 0)
                abstractUser = (ArrayList<AbstractUser>) getModelsObject(cursor
                        .getBlob(0));
        }
        cursor.close();
        db.close();
        close();

        return abstractUser;
    }

    public Object getModelsObject(byte[] bs) {

        try {
            ByteArrayInputStream bai=new ByteArrayInputStream(bs);

            ObjectInputStream ois = new ObjectInputStream(bai);
            Object details = ois.readObject();

            return details;

        } catch (StreamCorruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        return null;

    }

    public byte[] convertIntoBLOB(Object details){

        ByteArrayOutputStream bos=new  ByteArrayOutputStream();
        try {
            ObjectOutputStream ops = new ObjectOutputStream(bos);
            ops.writeObject(details);
            ops.close();
            bos.close();

            return bos.toByteArray();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
