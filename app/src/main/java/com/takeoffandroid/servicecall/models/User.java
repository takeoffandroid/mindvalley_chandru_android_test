package com.takeoffandroid.servicecall.models;

import org.json.*;

import java.io.Serializable;


public class User implements Serializable {
	
    private String username;
    private String id;
    private ProfileImage profileImage;
    private Links links;
    private String name;
    
    
	public User () {
		
	}	
        
    public User (JSONObject json) {
    
        this.username = json.optString("username");
        this.id = json.optString("id");
        this.profileImage = new ProfileImage(json.optJSONObject("profile_image"));
        this.links = new Links(json.optJSONObject("links"));
        this.name = json.optString("name");

    }
    
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ProfileImage getProfileImage() {
        return this.profileImage;
    }

    public void setProfileImage(ProfileImage profileImage) {
        this.profileImage = profileImage;
    }

    public Links getLinks() {
        return this.links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }


    
}
