package com.takeoffandroid.servicecall.models;

import org.json.*;

import java.io.Serializable;


public class Links implements Serializable {
	
    private String photos;
    private String likes;
    private String download;
    private String self;
    private String html;
    
    
	public Links () {
		
	}	
        
    public Links (JSONObject json) {
    
        this.photos = json.optString("photos");
        this.likes = json.optString("likes");
        this.download = json.optString("download");
        this.self = json.optString("self");
        this.html = json.optString("html");

    }
    
    public String getPhotos() {
        return this.photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }

    public String getLikes() {
        return this.likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getDownload() {
        return this.download;
    }

    public void setDownload(String download) {
        this.download = download;
    }

    public String getSelf() {
        return this.self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getHtml() {
        return this.html;
    }

    public void setHtml(String html) {
        this.html = html;
    }


    
}
