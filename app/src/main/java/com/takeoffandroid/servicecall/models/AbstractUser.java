package com.takeoffandroid.servicecall.models;

import org.json.*;

import java.io.Serializable;
import java.util.ArrayList;

public class AbstractUser implements Serializable {

    private String id;
    private ArrayList<Categories> categories;
    private String createdAt;
    private double width;
    private double likes;
    private Urls urls;
    private String color;
    private ArrayList<String> currentUserCollections;
    private Links links;
    private double height;
    private boolean likedByUser;
    private User user;


    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<Categories> getCategories() {
        return this.categories;
    }

    public void setCategories(ArrayList<Categories> categories) {
        this.categories = categories;
    }

    public String getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public double getWidth() {
        return this.width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLikes() {
        return this.likes;
    }

    public void setLikes(double likes) {
        this.likes = likes;
    }

    public Urls getUrls() {
        return this.urls;
    }

    public void setUrls(Urls urls) {
        this.urls = urls;
    }

    public String getColor() {
        return this.color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public ArrayList<String> getCurrentUserCollections() {
        return this.currentUserCollections;
    }

    public void setCurrentUserCollections(ArrayList<String> currentUserCollections) {
        this.currentUserCollections = currentUserCollections;
    }

    public Links getLinks() {
        return this.links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }

    public double getHeight() {
        return this.height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public boolean getLikedByUser() {
        return this.likedByUser;
    }

    public void setLikedByUser(boolean likedByUser) {
        this.likedByUser = likedByUser;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }


}
