package com.takeoffandroid.servicecall.models;

import org.json.*;

import java.io.Serializable;


public class Categories implements Serializable {
	
    private double id;
    private String title;
    private double photoCount;
    private Links links;
    
    
	public Categories () {
		
	}	
        
    public Categories (JSONObject json) {
    
        this.id = json.optDouble("id");
        this.title = json.optString("title");
        this.photoCount = json.optDouble("photo_count");
        this.links = new Links(json.optJSONObject("links"));

    }
    
    public double getId() {
        return this.id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPhotoCount() {
        return this.photoCount;
    }

    public void setPhotoCount(double photoCount) {
        this.photoCount = photoCount;
    }

    public Links getLinks() {
        return this.links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }


    
}
