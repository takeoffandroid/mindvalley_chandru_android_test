package com.takeoffandroid.servicecall.models;

import org.json.*;

import java.io.Serializable;


public class Urls implements Serializable {
	
    private String raw;
    private String full;
    private String thumb;
    private String regular;
    private String small;
    
    
	public Urls () {
		
	}	
        
    public Urls (JSONObject json) {
    
        this.raw = json.optString("raw");
        this.full = json.optString("full");
        this.thumb = json.optString("thumb");
        this.regular = json.optString("regular");
        this.small = json.optString("small");

    }
    
    public String getRaw() {
        return this.raw;
    }

    public void setRaw(String raw) {
        this.raw = raw;
    }

    public String getFull() {
        return this.full;
    }

    public void setFull(String full) {
        this.full = full;
    }

    public String getThumb() {
        return this.thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getRegular() {
        return this.regular;
    }

    public void setRegular(String regular) {
        this.regular = regular;
    }

    public String getSmall() {
        return this.small;
    }

    public void setSmall(String small) {
        this.small = small;
    }


    
}
