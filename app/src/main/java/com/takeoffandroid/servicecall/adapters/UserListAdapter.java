package com.takeoffandroid.servicecall.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.takeoffandroid.servicecall.R;
import com.takeoffandroid.servicecall.models.AbstractUser;
import com.takeoffandroid.servicecall.tasks.NetworkTasks;

import java.util.ArrayList;

public class UserListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    public ArrayList<AbstractUser> mUsersList;
    static OnItemClickListener mItemClickListener;
    private Typeface mTf;

    public UserListAdapter(Context context, ArrayList<AbstractUser> usersList, Typeface tf) {
        this.mContext = context;
        this.mUsersList = usersList;
        this.mTf = tf;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_users, parent, false);
        return new GenericViewHolder(v);
    }


    private AbstractUser getItem(int position) {
        return mUsersList.get(position);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            AbstractUser currentItem = getItem(position);

            GenericViewHolder genericViewHolder = (GenericViewHolder) holder;
            genericViewHolder.listTxtName.setText(currentItem.getUser().getName());


            NetworkTasks.loadImage(mContext, currentItem.getUrls().getSmall(), genericViewHolder.listImgUrl);


    }



    @Override
    public int getItemCount() {

        return mUsersList.size();

    }

    public void updateList(ArrayList<AbstractUser> filterList) {

        this.mUsersList = filterList;

        notifyDataSetChanged();
    }




    public class GenericViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView listImgUrl;
        private TextView listTxtName;


        public GenericViewHolder(View view) {
            super(view);
            listImgUrl = (ImageView) view.findViewById(R.id.list_img_url);
            listTxtName = (TextView) view.findViewById(R.id.list_txt_name);

            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {

            mItemClickListener.onItemClick(view, getAdapterPosition(), mUsersList.get(getAdapterPosition()));
        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position, AbstractUser abstractUser);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }


}
