package com.takeoffandroid.servicecall.util;

public class Constant {


    public static final int SPLASH_TIME_OUT = 1000;

    public interface URLS{

        String USER_LIST = "http://pastebin.com/raw/wgkJgazE";

    }


    public interface REQUEST_CODES{

        int REQUEST_CODE_USER_LIST = 100;
    }


    public interface SHARED_PREFS{


        String KEY_FLAG_MAINSCREEN = "screen_main";


    }


}
