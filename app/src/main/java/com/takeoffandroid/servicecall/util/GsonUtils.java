package com.takeoffandroid.servicecall.util;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.takeoffandroid.servicecall.models.AbstractUser;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by chandrasekar on 04/11/16.
 */

public class GsonUtils {


    /**
     * Method used to convert string into pojo (Model class object)
     * @param response
     * @param pojo
     * @param <T>
     * @return
     */
    public static <T> T createPojoFromString(String response, Class<T> pojo) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeNulls();
        return gsonBuilder.create().fromJson(response, pojo);
    }

    /**
     * Method to convert pojo (Model obect) into json string
     * @param context
     * @param data
     * @param <T>
     * @return
     */
    public static <T>String createJSONStringFromPojo(Context context, T data) {
        Gson gson = new Gson();
        return gson.toJson(data);

    }

    public static <T>ArrayList<AbstractUser> createPojoForUserArray(String response, Class<T> pojo) {

        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<AbstractUser>>() {
        }.getType();

       return gson.fromJson(response.toString(), type);


    }


}
