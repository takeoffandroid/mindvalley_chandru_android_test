package com.takeoffandroid.servicecall.util;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import java.util.Map;
import java.util.Map.Entry;


public class ActivityUtils {

   
    public static void launchActivity(Activity context, Class<? extends Activity> activity, boolean closeCurrentActivity, Map<String, String> params) {
        Intent intent = new Intent(context, activity);

        if (params != null) {
            Bundle bundle = new Bundle();
            for (Entry<String, String> param : params.entrySet()) {
                bundle.putString(param.getKey(), param.getValue());
            }
            intent.putExtras(bundle);
        }

        context.startActivity(intent);
        if (closeCurrentActivity) {
            context.finish();
        }
    }

   
    public static void launchActivity(Activity context, Class<? extends Activity> activity, boolean closeCurrentActivity) {
        ActivityUtils.launchActivity(context, activity, closeCurrentActivity, null);
    }

    
    public static String getExtraString(Activity context, String key) {
        String param = "";
        Bundle bundle = context.getIntent().getExtras();
        if (bundle != null) {
            param = bundle.getString(key);
        }
        return param;
    }

    
    public static Object getExtraObject(Activity context, String key) {
        Object param = null;
        Bundle bundle = context.getIntent().getExtras();
        if (bundle != null) {
            param = bundle.get(key);
        }
        return param;
    }


}
