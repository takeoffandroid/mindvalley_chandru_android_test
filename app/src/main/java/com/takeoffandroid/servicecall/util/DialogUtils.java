package com.takeoffandroid.servicecall.util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;

import com.takeoffandroid.servicecall.R;


/**
 * Created by chandrasekar.kuppusa on 24-07-2015.
 */
public class DialogUtils {



    public static void switchOnInternetDialog(final Context context, final boolean closeActivity){

        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Connection Failed");
        builder.setMessage("Please check your internet connection");
                builder.setPositiveButton("Settings", new DialogInterface.OnClickListener () {
                    @Override
                    public void onClick (DialogInterface dialog, int i) {
                        context.startActivity (new Intent (Settings.ACTION_SETTINGS));
                        dialog.dismiss();
                    }
                });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener () {
            @Override
            public void onClick (DialogInterface dialog, int i) {
                dialog.dismiss();
                if(closeActivity){
                    ((Activity)context).finish ();
                }

            }
        });


        builder.show();
    }

    public static void switchOnGPDDialog(final Context context, final boolean closeActivity){

        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Location Error");
        builder.setMessage("Unable to fetch your GPS Location. Please switch on Location services");
        builder.setPositiveButton("Settings", new DialogInterface.OnClickListener () {
            @Override
            public void onClick (DialogInterface dialog, int i) {
                context.startActivity (new Intent (Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener () {
            @Override
            public void onClick (DialogInterface dialog, int i) {
                dialog.dismiss();
                if(closeActivity){
                    ((Activity)context).finish ();
                }

            }
        });


        builder.show();
    }

    public static void showDialog(final Context context, String title,String message ){


        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener () {
            @Override
            public void onClick (DialogInterface dialog, int i) {

                dialog.dismiss ();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener () {
            @Override
            public void onClick (DialogInterface dialog, int i) {
                dialog.dismiss ();
            }
        });


        builder.show();
    }

    public static void showPositiveDialog(final Context context, String title,String message ){


        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener () {
            @Override
            public void onClick (DialogInterface dialog, int i) {

                dialog.dismiss ();


            }
        });



        builder.show();
    }

    public static void showPositiveDialog(final Context context, String title,String message, final boolean closeActivity ){


        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener () {
            @Override
            public void onClick (DialogInterface dialog, int i) {

                dialog.dismiss ();
                if(closeActivity){
                    ((Activity)context).finish ();
                }
            }
        });



        builder.show();
    }
}
