package com.takeoffandroid.servicecall.util;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

/**
 * Created by chandrasekar on 05/11/16.
 */

public class Utils {

    /**
     * Method to display Snacbar with short duration
     * @param view - View type (Eg: TextView, Button, EditText)
     * @param text - The text that should be shown on the Snackbar
     */
    public static void showSnackShort(View view, String text){
        Snackbar.make(view, text, Snackbar.LENGTH_SHORT).show();
    }


    /**
     * Method to show toast message
     * @param context
     * @param message
     */
    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
