package com.takeoffandroid.servicecall.modules.userdata;


import com.takeoffandroid.servicecall.listeners.VolleyTasksListener;

public interface UserDataPresenter {

    void fetchUserList();

    void onDestroy();
}