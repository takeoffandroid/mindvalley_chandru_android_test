package com.takeoffandroid.servicecall.modules.userdata;

import android.content.Context;
import android.support.design.widget.Snackbar;

import com.takeoffandroid.servicecall.R;
import com.takeoffandroid.servicecall.listeners.VolleyTasksListener;
import com.takeoffandroid.servicecall.tasks.NetworkTasks;
import com.takeoffandroid.servicecall.util.ConnectionUtils;
import com.takeoffandroid.servicecall.util.Constant;
import com.takeoffandroid.servicecall.util.Utils;


public class VolleyController implements UserDataPresenter {

    private VolleyTasksListener mVolleyTasksListener;
    private Context mContext;

    public VolleyController(Context context, VolleyTasksListener volleyTasksListener) {

        this.mContext = context;
        this.mVolleyTasksListener = volleyTasksListener;
    }


    @Override
    public void fetchUserList() {
        if (ConnectionUtils.isConnectedNetwork(mContext)) {
            NetworkTasks.callVolleyGETArray(mContext, mVolleyTasksListener, Constant.URLS.USER_LIST, Constant.REQUEST_CODES.REQUEST_CODE_USER_LIST);
        }else {

            mVolleyTasksListener.handleError(Constant.REQUEST_CODES.REQUEST_CODE_USER_LIST,mContext.getString(R.string.internet_connectivity_check));
            Utils.showToast(mContext,mContext.getString(R.string.internet_connectivity_check));
        }
    }

    @Override
    public void onDestroy() {
        mVolleyTasksListener = null;
    }


}