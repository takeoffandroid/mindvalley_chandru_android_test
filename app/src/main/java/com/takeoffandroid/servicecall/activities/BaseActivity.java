package com.takeoffandroid.servicecall.activities;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;


public class BaseActivity extends AppCompatActivity implements OnClickListener {
    private Typeface tfLight,tfRegular;

    @Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);

		getTypeface();
	}



	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public Typeface getTypeface(){
        tfLight = Typeface.createFromAsset (getAssets (), "fonts/font_regular.otf");

        return tfLight;
    }


}
