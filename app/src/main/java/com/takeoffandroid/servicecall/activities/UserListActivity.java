package com.takeoffandroid.servicecall.activities;

import android.app.SearchManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.takeoffandroid.servicecall.R;
import com.takeoffandroid.servicecall.adapters.UserListAdapter;
import com.takeoffandroid.servicecall.database.DBAccess;
import com.takeoffandroid.servicecall.models.AbstractUser;
import com.takeoffandroid.servicecall.modules.userdata.UserDataPresenter;
import com.takeoffandroid.servicecall.modules.userdata.VolleyController;
import com.takeoffandroid.servicecall.tasks.NetworkTasks;
import com.takeoffandroid.servicecall.listeners.VolleyTasksListener;
import com.takeoffandroid.servicecall.util.ConnectionUtils;
import com.takeoffandroid.servicecall.util.Constant;
import com.takeoffandroid.servicecall.util.DialogUtils;
import com.takeoffandroid.servicecall.util.GsonUtils;
import com.takeoffandroid.servicecall.util.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class UserListActivity extends BaseActivity implements VolleyTasksListener {


    private RecyclerView mRecyclerView;

    private UserListAdapter mAdapter;

    private LinearLayoutManager mLayoutManager;

    private Toolbar toolbar;
    private ArrayList<AbstractUser> mUsersList = new ArrayList<AbstractUser>();

    private SwipeRefreshLayout mSwipeRefresh;

    private UserDataPresenter userDataPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_user_list);
        findViews();
        toolBarData();

        userDataPresenter = new VolleyController(UserListActivity.this,this);

        mUsersList = DBAccess.init(this).getUserData();

        setAdapter();

        mSwipeRefresh.setOnRefreshListener (new SwipeRefreshLayout.OnRefreshListener () {
            @Override
            public void onRefresh () {

                userDataPresenter.fetchUserList();

            }
        });

    }


//    private void callUsersListService() {
//
//        if (ConnectionUtils.isConnectedNetwork(UserListActivity.this)) {
//            NetworkTasks.callVolleyGETArray(UserListActivity.this, Constant.URLS.USER_LIST, Constant.REQUEST_CODES.REQUEST_CODE_USER_LIST);
//        } else {
//            DialogUtils.switchOnInternetDialog(UserListActivity.this, true);
//        }
//    }


    @Override
    public void onResume() {
        super.onResume();


    }

    private void findViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_list);

        mSwipeRefresh = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh_list);
    }

    private void toolBarData() {

        toolbar.setTitle(getString(R.string.toolbar_title));

        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);


    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_search, menu);

        populateSearchView(menu);

        return true;
    }

    private void populateSearchView(Menu menu) {

        final SearchView searchView = (SearchView) MenuItemCompat
                .getActionView(menu.findItem(R.id.action_search));

        SearchManager searchManager = (SearchManager) this.getSystemService(this.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(this.getComponentName()));

        Drawable searchDrawable;

        if (Build.VERSION.SDK_INT >= 21) {
            searchDrawable = getResources().getDrawable(R.drawable.abc_ic_search_api_mtrl_alpha, null);
        } else {
            searchDrawable = getResources().getDrawable(R.drawable.abc_ic_search_api_mtrl_alpha);
        }
        searchDrawable.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);


        MenuItem settingsItem = menu.findItem(R.id.action_search);
        settingsItem.setIcon(searchDrawable);

        //changing edittext color
        EditText searchEdit = ((EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text));
        searchEdit.setTextColor(Color.WHITE);
        searchEdit.setHint("Search...");
        searchEdit.setHintTextColor(Color.WHITE);
        searchEdit.setCursorVisible(true);
        searchEdit.setFocusable(true);
        searchEdit.setFocusableInTouchMode(true);

        View v = searchView.findViewById(android.support.v7.appcompat.R.id.search_plate);
        v.setBackgroundColor(getResources().getColor(android.R.color.transparent));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {


                if (mUsersList != null && mUsersList.size() > 0) {
                    ArrayList<AbstractUser> filterList = new ArrayList<AbstractUser>();
                    if (s.length() > 0) {
                        for (int i = 0; i < mUsersList.size(); i++) {
                            if (mUsersList.get(i).getUser().getName().toLowerCase().contains(s.toString().toLowerCase())) {
                                filterList.add(mUsersList.get(i));
                                mAdapter.updateList(filterList);
                            }
                        }

                    } else {
                        mAdapter.updateList(mUsersList);
                    }
                }

                return false;
            }

        });
    }


    @Override
    public void handleError(int requestCode, String error) {

        Logger.i(error);
        stopRefreshing();

        DialogUtils.showPositiveDialog(this, "Connection Lost", "Sorry! Unable to fetch the data",false);
    }

    @Override
    public void handleResult(int requestCode, JSONObject response) {

    }

    @Override
    public void handleResult(int requestCode, JSONArray response) {

        if (requestCode == Constant.REQUEST_CODES.REQUEST_CODE_USER_LIST) {

            stopRefreshing();

             mUsersList = GsonUtils.createPojoForUserArray(response.toString(), AbstractUser.class);


            setAdapter();
        }


    }

    private void setAdapter() {
        if (mUsersList != null && mUsersList.size() > 0) {
            mAdapter = new UserListAdapter(this, mUsersList, getTypeface());


            mLayoutManager = new LinearLayoutManager(this);
            mRecyclerView.setLayoutManager(mLayoutManager);

            mRecyclerView.setAdapter(mAdapter);


            mAdapter.SetOnItemClickListener(new UserListAdapter.OnItemClickListener() {

                @Override
                public void onItemClick(View v, int position, AbstractUser abstractUser) {

                    Toast.makeText(UserListActivity.this, abstractUser.getUser().getName(),Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            DialogUtils.showPositiveDialog(this, "Alert", "Something went wrong!");

        }
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {

        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    public void stopRefreshing() {
        if(mSwipeRefresh.isRefreshing ()) {
            mSwipeRefresh.setRefreshing (false);
        }
    }

}
