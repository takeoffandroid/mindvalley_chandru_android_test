package com.takeoffandroid.servicecall.activities;

import android.os.Bundle;
import android.os.Handler;
import android.provider.SyncStateContract;

import com.takeoffandroid.servicecall.R;
import com.takeoffandroid.servicecall.database.DBAccess;
import com.takeoffandroid.servicecall.listeners.VolleyTasksListener;
import com.takeoffandroid.servicecall.models.AbstractUser;
import com.takeoffandroid.servicecall.modules.userdata.UserDataPresenter;
import com.takeoffandroid.servicecall.modules.userdata.VolleyController;
import com.takeoffandroid.servicecall.util.ActivityUtils;
import com.takeoffandroid.servicecall.util.Constant;
import com.takeoffandroid.servicecall.util.DialogUtils;
import com.takeoffandroid.servicecall.util.GsonUtils;
import com.takeoffandroid.servicecall.util.SharedPrefsUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class SplashActivity extends BaseActivity implements VolleyTasksListener {

    private Handler handler;
    private Runnable runnable;

    private UserDataPresenter userDataPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // This method will be executed once the timer is over
        // Start your app main activity

        userDataPresenter = new VolleyController(SplashActivity.this,this);

        userDataPresenter.fetchUserList();

        handler = new Handler();

        runnable = new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {


                ActivityUtils.launchActivity(SplashActivity.this, UserListActivity.class,true);

            }
        };

    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        handler.removeCallbacks(runnable);

        finish();
    }

    @Override
    public void onPause() {
        super.onPause();

        onBackPressed();
    }

    @Override
    public void handleError(int requestCode, String error) {

        DialogUtils.showPositiveDialog(this, "Connection Lost", "Sorry! Unable to fetch the data",true);


    }

    @Override
    public void handleResult(int requestCode, JSONArray response) {

        switch (requestCode){

            case Constant.REQUEST_CODES.REQUEST_CODE_USER_LIST:

                ArrayList<AbstractUser> usersList = GsonUtils.createPojoForUserArray(response.toString(), AbstractUser.class);

                DBAccess.init(SplashActivity.this).insertUserData(usersList);

                handler.postDelayed(runnable, Constant.SPLASH_TIME_OUT);

                break;
        }
    }

    @Override
    public void handleResult(int requestCode, JSONObject response) {

    }
}
